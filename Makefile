GCLOUD_VERSION ?= $(shell cat .version)

IMAGE_NAME ?= google-cloud-sdk-emulators
DOCKERHUB_IMAGE ?= fixl/$(IMAGE_NAME)
GITLAB_IMAGE ?= registry.gitlab.com/fixl/docker-$(IMAGE_NAME)

EMULATORS = bigtable datastore firestore pubsub spanner

BUILD_DATE ?= $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")

CI_COMMIT_SHORT_SHA ?= $(shell git rev-parse --short HEAD)
CI_PROJECT_URL ?= $(shell git config --get remote.origin.url)
CI_PIPELINE_URL ?= local

EXTRACTED_FILE = $(COMPONENT).tar
DOCKER_BUILDKIT = 1

TRIVY_COMMAND = docker compose run --rm trivy
ANYBADGE_COMMAND = docker compose run --rm anybadge

# Computed
BASE_IMAGE = gcr.io/google.com/cloudsdktool/cloud-sdk:$(GCLOUD_VERSION)-alpine

# create targets
build_targets = $(addprefix build_, $(EMULATORS))

scan_targets = $(addprefix scan_, $(EMULATORS))
extracted_file_targets = $(addsuffix .tar, $(EMULATORS))

publish_dockerhub_targets = $(addprefix publish_dockerhub_, $(EMULATORS))
publish_gitlab_targets = $(addprefix publish_gitlab_, $(EMULATORS))

badge_targets = $(addprefix badge_, $(EMULATORS))
clean_targets = $(addprefix clean_, $(EMULATORS))

version:
	bash version.sh

num_emulators:
	@echo $(words $(EMULATORS))

# Build images
build: $(build_targets)
$(build_targets): build_%: check_version
	docker buildx build \
		--progress=plain \
		--pull \
		--load \
		--build-arg GCLOUD_VERSION=$(GCLOUD_VERSION) \
		--label "org.opencontainers.image.title=$(*) emulator" \
		--label "org.opencontainers.image.url=https://cloud.google.com/sdk/gcloud/reference/beta/emulators/$(*)" \
		--label "org.opencontainers.image.authors=@fixl" \
		--label "org.opencontainers.image.version=$(GCLOUD_VERSION)" \
		--label "org.opencontainers.image.created=$(BUILD_DATE)" \
		--label "org.opencontainers.image.source=$(CI_PROJECT_URL)" \
		--label "org.opencontainers.image.revision=$(CI_COMMIT_SHORT_SHA)" \
		--label "info.fixl.gitlab.pipeline-url=$(CI_PIPELINE_URL)" \
		--tag $(IMAGE_NAME):$(*) \
		--tag $(GITLAB_IMAGE):$(*) \
		--tag $(GITLAB_IMAGE):$(GCLOUD_VERSION)-$(*) \
		--tag $(DOCKERHUB_IMAGE):$(*) \
		--tag $(DOCKERHUB_IMAGE):$(GCLOUD_VERSION)-$(*) \
			$(*)

# Scan for vulnerabilities
scan: $(scan_targets)
$(scan_targets): scan_%: check_version init_scan %.tar
	$(TRIVY_COMMAND) trivy image --input $(*).tar --exit-code 0 --no-progress --format template --template "@gitlab.tpl" -o $(*)-scan.json
	$(TRIVY_COMMAND) trivy image --input $(*).tar --exit-code 1 --no-progress --ignore-unfixed --severity CRITICAL

init_scan:
	if [ ! -f gitlab.tpl ] ; then curl --output gitlab.tpl https://raw.githubusercontent.com/aquasecurity/trivy/v$(shell docker compose run --rm trivy sh -c "trivy version" | grep Version | head -n1 | awk '{print $$2}')/contrib/gitlab.tpl;  fi
	$(TRIVY_COMMAND) trivy image --download-db-only

$(extracted_file_targets): %.tar:
	docker save --output $(*).tar $(IMAGE_NAME):$(*)

report:
	jq -s '{version: .[0].version, vulnerabilities: map(.vulnerabilities) | flatten, remediations: map(.remediations) | flatten }'  $(shell  ls *-scan.json) > gl-container-scanning-report.json

# Publish images to Gitlab
publish_gitlab: $(publish_gitlab_targets)
$(publish_gitlab_targets): publish_gitlab_%: check_version
	docker push $(GITLAB_IMAGE):$(*)
	docker push $(GITLAB_IMAGE):$(GCLOUD_VERSION)-$(*)

# Publish images to Dockerhub
publish_dockerhub: $(publish_dockerhub_targets)
$(publish_dockerhub_targets): publish_dockerhub_%: check_version
	docker push $(DOCKERHUB_IMAGE):$(*)
	docker push $(DOCKERHUB_IMAGE):$(GCLOUD_VERSION)-$(*)

# Create Badges
badges: $(badge_targets)
$(badge_targets): badge_%: check_version init_badges
	$(ANYBADGE_COMMAND) docker-size $(DOCKERHUB_IMAGE):$(GCLOUD_VERSION)-$(*) public/size_$(*)
	$(ANYBADGE_COMMAND) docker-version $(DOCKERHUB_IMAGE):$(GCLOUD_VERSION)-$(*) public/version_$(*)

init_badges: check_version
	mkdir -p public
	$(ANYBADGE_COMMAND) docker-version base:$(GCLOUD_VERSION) public/version

# Cleanup
clean: $(clean_targets)
$(clean_targets): clean_%: check_version
	-docker rmi $(IMAGE_NAME):$(*)
	-docker rmi $(GITLAB_IMAGE):$(*)
	-docker rmi $(GITLAB_IMAGE):$(GCLOUD_VERSION)-$(*)
	-docker rmi $(DOCKERHUB_IMAGE):$(*)
	-docker rmi $(DOCKERHUB_IMAGE):$(GCLOUD_VERSION)-$(*)

check_version:
ifeq ($(GCLOUD_VERSION),)
	$(error GCLOUD_VERSION not set)
endif
