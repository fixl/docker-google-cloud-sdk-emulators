# Google Cloud SDK Emulator Images

[![pipeline status](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/badges/master/pipeline.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/pipelines)
[![version](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/version.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
[![Docker Pulls](https://img.shields.io/docker/pulls/fixl/google-cloud-sdk-emulators)](https://hub.docker.com/r/fixl/google-cloud-sdk-emulators)
[![Docker Stars](https://img.shields.io/docker/stars/fixl/google-cloud-sdk-emulators)](https://hub.docker.com/r/fixl/google-cloud-sdk-emulators)

[Cloud SDK](https://cloud.google.com/sdk) images that only contain individual emulators.

## Emulators

Not all [Cloud SDK Emulators](https://cloud.google.com/sdk/gcloud/reference/beta/emulators/) come
installed with [google/cloud-sdk:latest](https://hub.docker.com/r/google/cloud-sdk).

## Tags

There is no `:latest` tag for any of the images contained in here. The latest version of a given
emulator is the group name of the emulator as returned by `gcloud beta emulators`. For a specific,
version the tag is `:<gcloud-version>-<group>`.

## Available Emulators

### Bigtable

[![version](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/version_bigtable.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
[![size](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/size_bigtable.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
- **Tags**: `:bigtable` and `:<gcloud-version>-bigtable`
- **Port**: Defaults to `8086`
- **Images**
  - Gitlab: `registry.gitlab.com/fixl/docker-google-cloud-sdk-emulators:bigtable`
  - Dockerhub: `fixl/google-cloud-sdk-emulators:bigtable`

### Datastore

[![version](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/version_datastore.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
[![size](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/size_datastore.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
- **Tags**: `:datastore` and `:<gcloud-version>-datastore`
- **Port**: Defaults to `8081`
- **Images**
  - Gitlab: `registry.gitlab.com/fixl/docker-google-cloud-sdk-emulators:datastore`
  - Dockerhub: `fixl/google-cloud-sdk-emulators:datastore`
- **Default Data Directory**: `/opt/datastore/data`

### Firestore

[![version](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/version_firestore.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
[![size](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/size_firestore.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
- **Tags**: `:firestore` and `:<gcloud-version>-firestore`
- **Port**: Defaults to `8080`
- **Images**
  - Gitlab: `registry.gitlab.com/fixl/docker-google-cloud-sdk-emulators:firestore`
  - Dockerhub: `fixl/google-cloud-sdk-emulators:firestore`

### Pub/Sub

[![version](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/version_pubsub.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
[![size](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/size_pubsub.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
- **Tags**: `:pubsub` and `:<gcloud-version>-pubsub`
- **Port**: Defaults to `8085`
- **Images**
  - Gitlab: `registry.gitlab.com/fixl/docker-google-cloud-sdk-emulators:pubsub`
  - Dockerhub: `fixl/google-cloud-sdk-emulators:pubsub`
- **Default Data Directory**: `/opt/pubsub/data`

### Spanner

[![version](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/version_spanner.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
[![size](https://fixl.gitlab.io/docker-google-cloud-sdk-emulators/size_spanner.svg)](https://gitlab.com/fixl/docker-google-cloud-sdk-emulators/-/commits/master)
- **Tags**: `:spanner` and `:<gcloud-version>-spanner`
- **Port**
  - Host Port: Defaults to `9010`
  - Rest Port: Defaults to `9020`
- **Images**
    - Gitlab: `registry.gitlab.com/fixl/docker-google-cloud-sdk-emulators:spanner`
    - Dockerhub: `fixl/google-cloud-sdk-emulators:spanner`
