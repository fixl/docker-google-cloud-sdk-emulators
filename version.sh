#!/usr/bin/env bash

set -e
set -x

VERSION_FILE=.version

LATEST_SDK_VERSION=$(curl -sSL https://gcr.io/v2/google.com/cloudsdktool/cloud-sdk/tags/list | jq --raw-output '.tags | sort_by(.) | map(select(.|test("^\\d+\\.\\d+\\.\\d+$"))) | .[-1]')

GITLAB_REPOSITORY_ID=$(curl -sSL https://gitlab.com/api/v4/projects/fixl%2Fdocker-google-cloud-sdk-emulators/registry/repositories | jq --raw-output '.[0].id')
GITLAB_LATEST_VERSION_RESPONSE=$(curl -sSL -o /dev/null -w "%{http_code}" https://gitlab.com/api/v4/projects/fixl%2Fdocker-google-cloud-sdk-emulators/registry/repositories/${GITLAB_REPOSITORY_ID}/tags/${LATEST_SDK_VERSION}-firestore)

# Check if one of the images with the latest available tags already exists in Gitlab
if [[ "${GITLAB_LATEST_VERSION_RESPONSE}" == "200" ]] ; then
    echo "Latest available version already built: ${LATEST_SDK_VERSION}"
    exit 1
fi

echo ${LATEST_SDK_VERSION} > ${VERSION_FILE}
